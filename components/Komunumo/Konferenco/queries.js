// Запрос для получения данных конкретного сообщества
export const komunumo_query = `
query($id: Float!) {
  komunumoj(objId: $id) {
    edges {
      node {
        id
        objId
        uuid
        tipo {
          kodo
          nomo {
            enhavo
          }
        }
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        statistiko {
          postulita
          tuta
          mia
          membraTipo
        }
        avataro {
          bildoE {
            url
          }
          bildoF {
            url
          }
        }
        kovrilo {
          bildoE {
            url
          }
        }
        informo {
          enhavo
        }
        informoBildo {
          bildo
          bildoMaks
        }
        kontaktuloj {
          edges {
            node {
              objId
              unuaNomo {
                enhavo
              }
              familinomo {
                enhavo
              }
              avataro {
                bildoF {
                  url
                }
              }
              kontaktaInformo
            }
          }
        }
        rajtoj
      }
    }
  }
}`;

// Запрос на получение списка категорий конференций
export const  konferenco_kategorioj_query = `
query($posedantoId: Float) {
  konferencojKategorioj(posedanto_Id: $posedantoId) {
    edges {
      node {
        uuid
        nomo {
          lingvo
          enhavo
          chefaVarianto
        } 
        posedanto {
          objId
        }
      }
    }
  }
}`;

// Запрос на получение тем категорий конференций
export const  konferenco_temoj_query = `
query($posedantoId: Float) {
  konferencojTemoj(posedanto_Id: $posedantoId) {
    edges {
      node {
        uuid
        nomo {
          lingvo
          enhavo
          chefaVarianto
        } 
        posedanto {
          objId
        }
      }
    }
  }
}`;

//Запрос для вступления пользователя в сообщество
export const kunigu_query = `
mutation($kom_uuid: UUID!) {
  kuniguKomunumon(komunumoUuid: $kom_uuid) {
    status
    message
    statistiko {
      postulita
      tuta
      mia
      membraTipo
    }
  }
}
`;

//Запрос для выхода пользователя из сообщества
export const forlasu_query = `
mutation($kom_uuid: UUID!) {
  forlasuKomunumon(komunumoUuid: $kom_uuid) {
    status
    message
    statistiko {
      postulita
      tuta
      mia
      membraTipo
    }
  }
}
`;